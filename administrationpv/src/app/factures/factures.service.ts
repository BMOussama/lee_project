import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Facture } from './factures';
import { FACTURES } from './mock-factures';

@Injectable({
  providedIn: 'root'
})
export class FactureService {

  constructor() { }


  getFactures(): Observable<Facture[]> {

    return of(FACTURES);
  }

  getFacture(id: number | string) {
    return this.getFactures().pipe(
      // (+) before `id` turns the string into a number
      map((factures: Facturess[]) => factures.find(facture => facture.id === +id))
    );
  }
}
