import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { FactureService }  from '../factures.service';
import { Facture } from '../factures';

@Component({
  selector: 'app-facture-list',
  templateUrl: './facture-list.component.html',
  styleUrls: ['./facture-list.component.css']
})
export class FactureListComponent implements OnInit {

  factures$: Observable<Facture[]>;
  selectedId: number;

  constructor(private service: FactureService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.factures$ = this.route.paramMap.pipe(
  switchMap(params => {
    console.log(params.get('id'));
    // (+) before `params.get()` turns the string into a number
    this.selectedId = +params.get('id');
    return this.service.getFactures();
  })
);
  }

}
