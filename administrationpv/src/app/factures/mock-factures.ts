import { Factures } from './factures';

export const FACTURES: Factures[] = [
  { id: 1, name: 'client1' },
  { id: 2, name: 'client2' },
  { id: 3, name: 'client3' },
  { id: 4, name: 'client4' },
  { id: 5, name: 'client5' }
];
