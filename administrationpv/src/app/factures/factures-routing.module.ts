import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureListComponent } from './facture-list/facture-list.component';
import { FactureDetailComponent } from './facture-detail/facture-detail.component';

const facturesRoutes: Routes = [
  { path: 'facture/:id', redirectTo: '/factures/:id' },
  { path: 'factures',  component: FactureListComponent },
  { path: 'factures/:id', component: FactureDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(facturesRoutes)],
  exports: [RouterModule]
})
export class FacturesRoutingModule { }
