import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { FacturesRoutingModule } from './factures-routing.module';

import { FactureListComponent } from './facture-list/facture-list.component';
import { FactureDetailComponent } from './facture-detail/facture-detail.component';

@NgModule({
  declarations: [FactureListComponent, FactureDetailComponent],
  imports: [
    CommonModule,
    FacturesRoutingModule,
    FormsModule
  ]
})
export class FacturesModule { }
