import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PartitionpvComponent } from './partitionpv/partitionpv.component';
import { ConnecteurresComponent } from './connecteurres/connecteurres.component';
import { PhotovComponent } from './photov/photov.component';
import { FormulaireConRsdComponent } from './formulaire-con-rsd/formulaire-con-rsd.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    PartitionpvComponent,
    ConnecteurresComponent,
    PhotovComponent,
    FormulaireConRsdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
