import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotovComponent } from './photov.component';

describe('PhotovComponent', () => {
  let component: PhotovComponent;
  let fixture: ComponentFixture<PhotovComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotovComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotovComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
