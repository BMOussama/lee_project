import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitionpvComponent } from './partitionpv.component';

describe('PartitionpvComponent', () => {
  let component: PartitionpvComponent;
  let fixture: ComponentFixture<PartitionpvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartitionpvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitionpvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
