import { Component,  Output,OnInit , EventEmitter} from '@angular/core';


@Component({
  selector: 'app-partitionpv',
  templateUrl: './partitionpv.component.html',
  styleUrls: ['./partitionpv.component.scss']
})
export class PartitionpvComponent implements OnInit {

  value: number = 0;
   @Output() counterChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  increment() {
  this.value=0;

}
decrement() {
  this.value=1;
}

}
