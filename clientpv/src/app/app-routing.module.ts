import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnecteurresComponent }      from './connecteurres/connecteurres.component';
import { PartitionpvComponent }      from './partitionpv/partitionpv.component';
import { FormulaireConRsdComponent }      from './formulaire-con-rsd/formulaire-con-rsd.component';

const routes: Routes = [
  { path: '', redirectTo : '/partitionpv', pathMatch : 'full' },
  { path: 'partitionpv', component: PartitionpvComponent },
  { path: 'formulaire', component: FormulaireConRsdComponent },
  { path: 'connecteurres', component: ConnecteurresComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
