import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnecteurresComponent } from './connecteurres.component';

describe('ConnecteurresComponent', () => {
  let component: ConnecteurresComponent;
  let fixture: ComponentFixture<ConnecteurresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnecteurresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnecteurresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
