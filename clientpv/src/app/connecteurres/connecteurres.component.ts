import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-connecteurres',
  templateUrl: './connecteurres.component.html',
  styleUrls: ['./connecteurres.component.scss']
})
export class ConnecteurresComponent implements OnInit {

  constructor(  private route : ActivatedRoute,private location : Location) { }

  ngOnInit() {
  }
  goBack(): void{
    this.location.back();
  }
}
