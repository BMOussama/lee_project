import { TestBed } from '@angular/core/testing';

import { FormcrServiceService } from './formcr-service.service';

describe('FormcrServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormcrServiceService = TestBed.get(FormcrServiceService);
    expect(service).toBeTruthy();
  });
});
