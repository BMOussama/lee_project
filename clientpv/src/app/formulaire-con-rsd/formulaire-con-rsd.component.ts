import { Component, OnInit } from '@angular/core';
import { Varformulaire }from './varformulaire';
import {ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormcrService } from './formcr-service.service';

import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-formulaire-con-rsd',
  templateUrl: './formulaire-con-rsd.component.html',
  styleUrls: ['./formulaire-con-rsd.component.scss']
})
export class FormulaireConRsdComponent implements OnInit {

  submitted = false;
  model = new Varformulaire();
  success=false;

  constructor(private route : ActivatedRoute,
    private location : Location,
    private formcrService: FormcrService
    ,private http:HttpClient
  ) { }

  ngOnInit() {
  }


  add(): void {

    this.formcrService.addConRes( this.model )
      .subscribe();
  }



  onSubmit() {
    this.submitted = true;
  }

  postData() {
  this.http
    .post("http://localhost:3000", {name:this.model.name,
    prenom : this.model.prenom,tel: this.model.telephone,
    mail:this.model.mail,cons:this.model.cons,
    address:{nrue:this.model.address.nrue,
    rue:this.model.address.rue,
    ville:this.model.address.ville,
   cp:this.model.address.cp}},{ responseType: 'text' })
    .subscribe(res => {
      var data = JSON.parse(res)
      if (data.success){
        this.success = true
      }
    });
  }

  showFormControls(form: any) {
    return form && form.controls['name'] &&
    form.controls['name'].value;
  }
  goBack(): void{
    this.location.back();
  }

  test(){
    console.log(this.model.name)
    this.formcrService.test()
  }



}
