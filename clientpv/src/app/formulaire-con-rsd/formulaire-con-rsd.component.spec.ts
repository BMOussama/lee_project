import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireConRsdComponent } from './formulaire-con-rsd.component';

describe('FormulaireConRsdComponent', () => {
  let component: FormulaireConRsdComponent;
  let fixture: ComponentFixture<FormulaireConRsdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireConRsdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireConRsdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
