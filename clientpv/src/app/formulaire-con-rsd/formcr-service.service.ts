import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {Varformulaire} from './varformulaire';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class FormcrService{
  public reponse:string;

  private ConResUrl = 'localhost:3000';

  constructor(private http: HttpClient){
  }

  addConRes (varformulaire: Varformulaire): Observable<Varformulaire> {
  return this.http.post<Varformulaire>(this.ConResUrl,varformulaire, httpOptions).pipe(
    tap(_ => this.log(`updated hero `)),catchError(this.handleError<any>('updateHero'))
  );
  }

  test(){
    return this.http.post(this.ConResUrl,{zok:'zok'},httpOptions ).pipe(
      tap(_ => this.log(`updated hero `)),catchError(this.handleError<any>('updateHero'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.reponse=`HeroService: ${message}`;
  }
}
